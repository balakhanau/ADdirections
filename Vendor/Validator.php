<?php

/* created by Vadim.B 2018.27.2
    do all main validations for input before start to calculate
*/

namespace Root\Vendor;

include 'Vendor/Refactor.php';

class Validator{

    use Refactor;

    public $_validateResult = array('validated'=>true, 'message'=>"");
    public static $_actionsList = array('walk','turn');

    CONST LOWEST_VALIDATION_COUNT = 2;
    CONST MAX_PERSON_COUNT = 20;
    CONST MIN_INSTRUCTION_VALUES_COUNT = 4;
    CONST MAX_INSTRUCTION_EACH_PERSON = 25;
    CONST MAX_TEST_CASE_COUNT = 100;
    CONST MAX_DISTANCE_COUNT = 1000;



    /*
     * validates all input before calculation
     * @param input string
     * return array
     */
    public function validateInput($input){

        $inputArray = $this->upgradeInput($input);
        if($this->validateEmpty($inputArray)) {
            if ($this->validateEndOfInput($inputArray)) {
                $this->checkIntegrity($inputArray);
            }
        }
        return $this->_validateResult;


    }



    /*
     * validates integrity of all input
     * @param inputArray array
     * return array
     */
    private function checkIntegrity($inputArray){
        $isPersonCount = true;
        $testCaseCount = 0;
        $zeroFlag = false;
        $needCount = 0;
        $currentCount = 0;
        foreach ($inputArray as $key=>$value){
            if(!$zeroFlag) {
                if($testCaseCount<=self::MAX_TEST_CASE_COUNT) {
                    if ($isPersonCount) {
                        $currentCount = 0;
                        $isPersonCount = false;
                        $testCaseCount += 1;

                        $personCount = trim($value);
                        if ($personCount < 0 || $personCount > self::MAX_PERSON_COUNT || !preg_match("/^\d+$/", $personCount)) {
                            $this->_validateResult['validated'] = false;
                            $this->_validateResult['message'] = $value . ' is incorrect count of persons! It has to be positive integer and less then ' . self::MAX_PERSON_COUNT;
                            break;
                        } else if ($personCount == 0) {
                            $zeroFlag = true;
                        } else {
                            $needCount = $personCount;
                        }


                    } else {
                        $currentCount += 1;
                        $currentOption = trim($value);
                        if (!$this->isSingleOption($currentOption)) {
                            break;
                        }


                        if ($currentCount == $needCount) {
                            $isPersonCount = true;
                        }

                    }
                }
                else{
                    $this->_validateResult['validated'] = false;
                    $this->_validateResult['message'] = 'Max test case count is '.self::MAX_TEST_CASE_COUNT;
                    break;
                }
            }else{
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Stop flag 0 has been set incorrectly or 2 times. Check your Input!';
                break;
            }
        }

        return $this->_validateResult;
    }

    /*
     * checks if single person operator is coorrect
     * @param option string
     * return boolean
     */
    private function isSingleOption($option){
        $optionArray = explode(" ", $option);
        $optionValueCounter = 1;
        $instructionsCounter = 0;
        $previousValue = null;
        //trait in  usage
        $optionArray = $this->excludeEmptySymbol($optionArray,"");

        if(count($optionArray)>=self::MIN_INSTRUCTION_VALUES_COUNT) {
            foreach ($optionArray as $optionValue) {
                if ($optionValueCounter == 1 || $optionValueCounter == 2) {
                    if (!$this->validateCoordinate($optionValue, $option)) {
                        return false;
                    }
                } else if ($optionValueCounter == 3) {
                    if (!$this->validateStart($optionValue,$option)) {
                        return false;
                    }
                } else if($optionValueCounter==4){
                    if(!$this->validateAngle($optionValue,$option)){
                        return false;
                    }
                } else {
                    if($previousValue==null || !$this->isInActionsList($previousValue)){
                        if(!$this->isInActionsList($optionValue)){
                            $this->_validateResult['validated'] = false;
                            $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! WALK or TURN instruction is missed.';
                            return false;
                        }
                        else{
                            if($instructionsCounter<=self::MAX_INSTRUCTION_EACH_PERSON) {
                                $instructionsCounter += 1;
                            } else {
                                $this->_validateResult['validated'] = false;
                                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! Max instructions count for each person is '.self::MAX_INSTRUCTION_EACH_PERSON;
                                return false;
                            }
                        }
                    }
                    else{
                        if($previousValue=='turn'){
                            if(!$this->validateAngle($optionValue,$option)){
                                return false;
                            }
                        } else{
                            if ($previousValue=='walk'){
                                if (!$this->validateDistance($optionValue, $option)) {
                                    return false;
                                }
                            }
                        }
                    }
                    $previousValue = $optionValue;
                }
            $optionValueCounter+=1;
            }
        }
        else{
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Instruction '.$option.' has invalid count of operators('.self::MIN_INSTRUCTION_VALUES_COUNT.') or instructions count is not relevant with asked persons count!';
            return false;
        }
        return true;
    }

    /*
     * coordinate validator
     * @param value float
     * @param option string
     * return boolean
     */

    private function validateCoordinate($value, $option){
        if(!is_numeric($value)){
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is not numeric!';
            return false;
        }
        else {
            $value = floatval($value);
            if(floatval($value)<-1000 || floatval($value)>1000){
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is out of range [-1000,1000]';
                return false;

            } else if(!preg_match("/^-?\d{1,3}(\.\d{1,4})?$/",$value)){
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is incorrect due to the issue real number requirements';
                return false;

            }else{
                return true;
            }
        }
    }

    /*
     * start operator validator
     * @param value string
     * @param option string
     * return boolean
     */
    private function validateStart($value, $option){
        if($value!='start'){
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! instruction START has to go after start coordinates';
            return false;
        }
        else{
            return true;
        }
    }

    /*
     * angle validator
     * @param value float
     * @param option string
     * return boolean
     */
    private function validateAngle($value, $option){
        if(!is_numeric($value)){
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is not numeric!';
            return false;
        }
        else {
            $value = floatval($value);
            if(floatval($value)<-360 || floatval($value)>360){
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is out of range [-360,360]';
                return false;

            } else if(!preg_match("/^-?\d{1,3}(\.0)?$/",$value)){
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is incorrect due to the issue real number requirements';
                return false;

            }else{
                return true;
            }
        }
    }

    /*
     * distance validator
     * @param value float
     * @param option string
     * return boolean
     */
    private function validateDistance($value, $option){
        if(!is_numeric($value)){
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is not numeric!';
            return false;
        }
        else {
            $value = floatval($value);
            if(floatval($value)<=0 || floatval($value)>1000){
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is out of range [1,1000]';
                return false;

            } else if(!preg_match("/^\d{1,3}(\.\d{1,4})?$/",$value)){
                $this->_validateResult['validated'] = false;
                $this->_validateResult['message'] = 'Instruction '.$option.' is incorrect! '.$value.' is incorrect due to the issue number requirements';
                return false;

            }else{
                return true;
            }
        }
    }


    /*
     * checks if value is valid instruction operator
     * @param value string
     * return boolean
     */
    private function isInActionsList($value){
        if(in_array($value, self::$_actionsList)){
            return true;
        }
        else{
            return false;
        }
    }

    /*
    * create array from input string
    * @param input string
    * return array
    */
    public function upgradeInput($input){

        //replace windows linux mac \r\n
        $input = str_replace(array("\r\n","\n", "\r"), "%rn%", $input);
        $inputArray = explode("%rn%", $input);
        //trait in  usage
        $inputArray = $this->excludeEmptySymbol($inputArray,"");

        return $inputArray;
    }

    /*
   * validates if the input has finish flag
   * @param inputArray array
   * return boolean
   */
    private function validateEndOfInput($inputArray){
        if(array_reverse($inputArray)[0] != "0"){
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Input has to end with zero';
            return false;
        }
        return true;
    }

    /*
     * validates if the input is not empty
     * @param inputArray array
     * return boolean
     */
    private function validateEmpty($inputArray){
        if(empty($inputArray) || count($inputArray)<=self::LOWEST_VALIDATION_COUNT){
            $this->_validateResult['validated'] = false;
            $this->_validateResult['message'] = 'Input does not relevant to the  row count requirements';
            return false;
        }
        return true;
    }
}
