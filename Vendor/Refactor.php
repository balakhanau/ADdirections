<?php

namespace Root\Vendor;

trait Refactor{

    /*
     * exclude all unnecessary symbols from array if it was got a lot of spaces during input
     * @param source array
     * @param symbol string
     * return array
     */
    public function excludeEmptySymbol($source, $symbol){
        foreach (array_keys($source, $symbol, true) as $key){
            unset($source[$key]);
        }

        return $source;

    }
}