<?php
/* created by Vadim.B 2018.28.2
*/

namespace Root\Vendor;




class Calculator{

    use Refactor;

    public $_outputStr = "";
    public $_analysisArray = array();

    /*calculate all necessary operations with input
    * @param inputArray array
    * return string
    */


    public function calculate($inputArray){
        $isPersonCount = true;
        $testCaseCount = 0;
        $zeroFlag = false;
        $needCount = 0;
        $currentCount = 0;
        $oneFullInstruction = array();
        foreach ($inputArray as $key=>$value) {
            if (!$zeroFlag) {

                if ($isPersonCount) {
                    $currentCount = 0;
                    $isPersonCount = false;
                    $testCaseCount += 1;
                    if(!empty($oneFullInstruction)) {
                        $this->_analysisArray[$testCaseCount] = $oneFullInstruction;
                    }
                    $oneFullInstruction = array();

                   $personCount = trim($value);
                   if ($personCount == 0) {
                        $zeroFlag = true;
                   } else {
                        $needCount = $personCount;
                   }


                } else {
                    $currentCount += 1;
                    $currentOption = trim($value);
                    $oneFullInstruction[] = $this->calculateSingleDestination($currentOption);

                    if ($currentCount == $needCount) {
                        $isPersonCount = true;
                    }

                }

            }
        }

        foreach ($this->_analysisArray as $key=>$currentArray){
            $sumX = 0;
            $sumY = 0;
            foreach ($currentArray as $keyD=>$destination){
                $sumX+=$destination['X'];
                $sumY+=$destination['Y'];
            }

            $averageX = round($sumX/count($currentArray),4);
            $averageY = round($sumY/count($currentArray),4);

            $distances  = array();

            foreach($currentArray as $keyD1=>$destination1){
                $distances[] = $this->getDistance($averageX,$destination1['X'],$averageY,$destination1['Y']);
            }



            $this->_outputStr .= $averageX." ".$averageY." ".max($distances)."\r\n";
        }

        return $this->_outputStr;

    }

    /*
     * calculate destination coordinates of single person instruction and last angle value+last operation
     * @param option string
     * return array
     */

    private function calculateSingleDestination($option){
        $optionArray = explode(" ", $option);

        $optionArray = $this->excludeEmptySymbol($optionArray,"");

        $optionValueCounter = 1;
        $instructionsCounter = 0;
        $currentPosition = array();
        foreach ($optionArray as $value){

            if($optionValueCounter==1){
                $currentPosition['X'] = floatval($value);

            } else
            if($optionValueCounter==2){
                $currentPosition['Y'] = floatval($value);
            } else
            if($optionValueCounter==4){
                $currentPosition['Angle'] = floatval($value);
            } else{
                //3rd element is always start operator
                if($optionValueCounter!=3){
                   if(in_array($value,Validator::$_actionsList)){
                       $currentPosition['Operator'] = $value;
                   } else{
                       $operand = floatval($value);
                       if($currentPosition['Operator']=='walk'){
                            $currentPosition['X'] = $currentPosition['X']+$value*cos($this->radiant($currentPosition['Angle']));
                            $currentPosition['Y'] = $currentPosition['Y']+$value*sin($this->radiant($currentPosition['Angle']));


                       } else if($currentPosition['Operator']=='turn'){
                           $currentPosition['Angle'] +=$value;
                       }
                   }
                }
            }



            $optionValueCounter+=1;
        }

        return $currentPosition;

    }

    /*
     * count angle value in radiant
     * @param value float
     * return float
     */

    private function radiant($value){

        $value  = $value*M_PI/180;
        return $value;

    }


    /*
     * count distance between 2 coordinates
     * @param x1 float
     * @param x2 float
     * @param y1 float
     * @param y2 float
     * return float
     */
    private function getDistance($x1,$x2,$y1,$y2){
        return round(sqrt(pow(($x1-$x2),2)+pow(($y1-$y2),2)),5);
    }



}