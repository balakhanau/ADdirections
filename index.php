<?php
namespace Root;

use \Root\Controllers as Controllers;

include 'Controllers/IndexController.php';

$indexClass = new Controllers\IndexController();

echo $indexClass->index();