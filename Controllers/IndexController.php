<?php

namespace Root\Controllers;

use \Root\Vendor as Vendor;

include 'Vendor/Validator.php';
include 'Vendor/Calculator.php';
include 'Vendor/Template.php';

class IndexController {

    public function index() {



        $view = new Vendor\Template();
        $view->errorMessage = null;
        $input = null;
        $output = null;


        if (isset($_POST['input'])){
            $input = trim($_POST['input']);
            $validator = new Vendor\Validator();
            $validateResult = $validator->validateInput($input);
            if($validateResult['validated']){
                $output = $this->calculate($input);
            }
            else{
                $view->errorMessage = $validateResult['message'];
            }
        }
        $view->inputm = $input;
        $view->outputm = $output;

        echo $view->render('Views/index.html');


    }

    private  function calculate($input){
        $validator = new Vendor\Validator();
        $calculator = new Vendor\Calculator();
        $inputArray = $validator->upgradeInput($input);
        return $calculator->calculate($inputArray);

    }
}

